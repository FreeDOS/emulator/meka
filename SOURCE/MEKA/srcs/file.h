//-----------------------------------------------------------------------------
// MEKA - file.h
// ROM File Loading & File Tools - Headers
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Definitions
//-----------------------------------------------------------------------------

#define LOAD_COMMANDLINE  (0)
#define LOAD_INTERFACE    (1)

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void            Load_ROM                (int mode, int user_verbose);
void            Load_ROM_Command_Line   (void);

void            Filenames_Init          (void); // Initialize emulator filenames

//-----------------------------------------------------------------------------

