/* Allegro datafile object indexes, produced by grabber v4.1.9 (WIP), MSVC.s */
/* Datafile: e:\meka\meka-wip.dat */
/* Date: Sat Apr 03 15:14:11 2004 */
/* Do not hand edit! */

#define DATA_CURSOR_LIGHTPHASER          0        /* BMP  */
#define DATA_CURSOR_MAIN                 1        /* BMP  */
#define DATA_CURSOR_SPORTSPAD            2        /* BMP  */
#define DATA_CURSOR_TVOEKAKI             3        /* BMP  */
#define DATA_CURSOR_WAIT                 4        /* BMP  */
#define DATA_FLAG_AU                     5        /* BMP  */
#define DATA_FLAG_BR                     6        /* BMP  */
#define DATA_FLAG_CH                     7        /* BMP  */
#define DATA_FLAG_DE                     8        /* BMP  */
#define DATA_FLAG_EU                     9        /* BMP  */
#define DATA_FLAG_FR                     10       /* BMP  */
#define DATA_FLAG_HK                     11       /* BMP  */
#define DATA_FLAG_IT                     12       /* BMP  */
#define DATA_FLAG_JP                     13       /* BMP  */
#define DATA_FLAG_KR                     14       /* BMP  */
#define DATA_FLAG_NZ                     15       /* BMP  */
#define DATA_FLAG_PT                     16       /* BMP  */
#define DATA_FLAG_SP                     17       /* BMP  */
#define DATA_FLAG_SW                     18       /* BMP  */
#define DATA_FLAG_UK                     19       /* BMP  */
#define DATA_FLAG_UNKNOWN                20       /* BMP  */
#define DATA_FLAG_US                     21       /* BMP  */
#define DATA_FONT_0                      22       /* FONT */
#define DATA_FONT_1                      23       /* FONT */
#define DATA_FONT_2                      24       /* FONT */
#define DATA_GFX_DRAGON                  25       /* BMP  */
#define DATA_GFX_GLASSES                 26       /* BMP  */
#define DATA_GFX_HEART1                  27       /* BMP  */
#define DATA_GFX_HEART2                  28       /* BMP  */
#define DATA_GFX_INPUTS                  29       /* BMP  */
#define DATA_GFX_JOYPAD                  30       /* BMP  */
#define DATA_GFX_KEYBOARD                31       /* BMP  */
#define DATA_GFX_LIGHTPHASER             32       /* BMP  */
#define DATA_GFX_PADDLECONTROL           33       /* BMP  */
#define DATA_GFX_SPORTSPAD               34       /* BMP  */
#define DATA_GFX_SUPERHEROPAD            35       /* BMP  */
#define DATA_GFX_TVOEKAKI                36       /* BMP  */
#define DATA_ICON_BAD                    37       /* BMP  */
#define DATA_ICON_BIOS                   38       /* BMP  */
#define DATA_ICON_HACK                   39       /* BMP  */
#define DATA_ICON_HOMEBREW               40       /* BMP  */
#define DATA_ICON_PROTO                  41       /* BMP  */
#define DATA_ICON_TRANS_JP               42       /* BMP  */
#define DATA_ICON_TRANS_JP_US            43       /* BMP  */
#define DATA_MACHINE_COLECO              44       /* BMP  */
#define DATA_MACHINE_SMS                 45       /* BMP  */
#define DATA_MACHINE_SMS_CART            46       /* BMP  */
#define DATA_MACHINE_SMS_LIGHT           47       /* BMP  */
#define DATA_ROM_COLECO                  48       /* DATA */
#define DATA_ROM_SF7000                  49       /* DATA */
#define DATA_ROM_SMS                     50       /* DATA */
#define DATA_ROM_SMS_J                   51       /* DATA */
#define DATA_COUNT                       52

