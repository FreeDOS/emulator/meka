//-----------------------------------------------------------------------------
// MEKA - effects.h
// Various effects - Headers
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void    Effects_Init_Values (void);
void    Effects_TV_Update (void);
void    Effects_TV_Init_Colors (void);

//-----------------------------------------------------------------------------
// Data
//-----------------------------------------------------------------------------

typedef struct
{
  int   TV_Enabled;
  int   TV_Start_Line;
} t_effects;

t_effects       effects;

//-----------------------------------------------------------------------------

